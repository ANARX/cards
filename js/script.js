const rootDiv = document.querySelector("#item");
const mainDiv = document.querySelector("#main");
const loginBtn = document.querySelector("#login");
loginBtn.addEventListener("click", () => {
    authorization();
});
const createBtn = document.querySelector("#createCard");
createBtn.addEventListener("click", () => {
    mainDiv.classList.add("active");
});
const modalBtn = document.querySelectorAll(".modalCreate_btn");
let token = "bfc13466a192";
const login = "balcksunn@mail.com";
const password = "cGvuM4w6cBuRi82";

function authorization() {
    const URL = "http://cards.danit.com.ua/login",
        authorizationData = {
            email: login,
            password: password
        },
        requestData = fetch(URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(authorizationData)
        })
            .then((response) => response.json())
            .then((result) => {
                if (result.status === "Success") {
                    token = result.token;
                    createBtn.classList.add("active");
                    loginBtn.classList.remove("active");
                    getCard();
                } else if (result.status === "Error") {
                    alert("Error");
                } else {
                    alert("Error authorization");
                }
            });
}

function setCard() {
    const parentDiv = event.target.closest(".modalDoc");
    const typeDoctor = parentDiv.querySelector("[name=modalSelect]");
    const userVisit = parentDiv.querySelector("[name=userVisit]").value;
    const userDesc = parentDiv.querySelector("[name=userDesc]").value;
    const userDoc = parentDiv.querySelector("[name=userDoc]").options[
        parentDiv.querySelector("[name=userDoc]").selectedIndex
        ].text;
    const visitPriority = parentDiv.querySelector("[name=visitPriority]").value;
    const userName = parentDiv.querySelector("[name=userName]").value;
    const userPressure = parentDiv.querySelector("[name=userPressure]")
        ? parentDiv.querySelector("[name=userPressure]").value
        : null;
    const userBodyMass = parentDiv.querySelector("[name=userBodyMass]")
        ? parentDiv.querySelector("[name=userBodyMass]").value
        : null;
    const userIllnesses = parentDiv.querySelector("[name=userIllnesses]")
        ? parentDiv.querySelector("[name=userIllnesses]").value
        : null;
    const lastDate = parentDiv.querySelector("[name=lastDate]")
        ? parentDiv.querySelector("[name=lastDate]").value
        : null;
    const userAge = parentDiv.querySelector("[name=userAge]")
        ? parentDiv.querySelector("[name=userAge]").value
        : null;
    const data = {};

    switch (typeDoctor.value) {
        case "cardiologist":
            data.typeDoctor = typeDoctor.value;
            data.userVisit = userVisit;
            data.userDesc = userDesc;
            data.userDoc = userDoc;
            data.visitPriority = visitPriority;
            data.userName = userName;
            data.userPressure = userPressure;
            data.userBodyMass = userBodyMass;
            data.userIllnesses = userIllnesses;
            data.userAge = userAge;
            data.status = "open";
            break;

        case "dentist":
            data.typeDoctor = typeDoctor.value;
            data.userVisit = userVisit;
            data.userDesc = userDesc;
            data.userDoc = userDoc;
            data.visitPriority = visitPriority;
            data.userName = userName;
            data.lastDate = lastDate;
            data.status = "open";
            break;

        case "therapist":
            data.typeDoctor = typeDoctor.value;
            data.userVisit = userVisit;
            data.userDesc = userDesc;
            data.userDoc = userDoc;
            data.visitPriority = visitPriority;
            data.userName = userName;
            data.userAge = userAge;
            data.status = "open";
            break;
    }

    const URL = "http://cards.danit.com.ua/cards",
        cardRequest = fetch(URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((result) => {
                console.log(result);

                if (result.id) {
                    createCard(result);
                } else if (result.status === "Error") {
                    alert("Error");
                } else {
                    alert("Error authorization");
                }
            });
}

function createCard(obj) {
    let card = null;

    switch (obj.typeDoctor) {
        case "cardiologist":
            card = new Cardiologist(
                obj.id,
                obj.userVisit,
                obj.userDesc,
                obj.userDoc,
                obj.visitPriority,
                obj.userName,
                obj.userPressure,
                obj.userBodyMass,
                obj.userIllnesses,
                obj.userAge,
                obj.status
            );
            card.render();
            break;

        case "dentist":
            card = new Dentist(
                obj.id,
                obj.userVisit,
                obj.userDesc,
                obj.userDoc,
                obj.visitPriority,
                obj.userName,
                obj.lastDate,
                obj.status
            );
            card.render();
            break;

        case "therapist":
            card = new Therapist(
                obj.id,
                obj.userVisit,
                obj.userDesc,
                obj.userDoc,
                obj.visitPriority,
                obj.userName,
                obj.userAge,
                obj.status
            );
            card.render();
            break;
    }
}

function deleteCard(id) {
    const URL = `http://cards.danit.com.ua/cards/${id}`;
    const cardRequest = fetch(URL, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: `Bearer ${token}`
        }
    })
        .then((response) => response.json())
        .then((result) => {
            if (result.status === "Success") {
                const deleteElement = document.getElementById(id);
                deleteElement.remove();
            } else if (result.status === "Error") {
                alert("Error");
            } else {
                alert("Error delete card");
            }
        });
}

function getCard() {
    const URL = "http://cards.danit.com.ua/cards",
        cardRequest = fetch(URL, {
            method: "GET",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                Authorization: `Bearer ${token}`
            }
        })
            .then((response) => response.json())
            .then((result) => {
                if (result.length > 0) {
                    console.log(result);
                    result.forEach((item) => {
                        createCard(item);
                        checkEmpty();
                    });
                } else {
                    checkEmpty();
                }
            });
}

function checkEmpty() {
    const item = document.querySelector("#empty");
    rootDiv.childElementCount
        ? item.classList.remove("active")
        : item.classList.add("active");
}

function setStatus(id, status) {
    const data = {
            status
        },
        URL = `http://cards.danit.com.ua/cards/${id}`,
        cardRequest = fetch(URL, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify(data.status)
        })
            .then((response) => response.json())
            .then((result) => {
                console.log(result);

                if (result.status === "Success") {
                } else if (result.status === "Error") {
                    alert("Error");
                } else {
                    alert("Error set status card");
                }
            });
}

modalBtn.forEach((btn) => {
    btn.addEventListener("click", (event) => {
        setCard();
    });
});

class Visit {
    constructor(
        id,
        userVisit,
        userDesc,
        userDoc,
        visitPriority,
        userName,
        status
    ) {
        this.id = id;
        this.idLabel = "ID";
        this.userVisit = userVisit;
        this.userVisitLabel = "Visit Purpose";
        this.userDesc = userDesc;
        this.userDescLabel = "Description";
        this.userDoc = userDoc;
        this.userDocLabel = "Doctor";
        this.visitPriority = visitPriority;
        this.priorityLabel = "Priority";
        this.userName = userName;
        this.userNameLabel = "Name";
        this.status = status;
        this.visitCard = null;
        this.visitCardInfo = null;
        this.offsetX = 0;
        this.offsetY = 0;
    }

    set setStatus(state) {
        if (state === "open") {
            this.visitCard.classList.add("open");
            this.visitCard.classList.remove("done");
        } else {
            this.visitCard.classList.remove("open");
            this.visitCard.classList.add("done");
        }
    }

    nulledIndex() {
        const item = rootDiv.querySelectorAll(".cardJs");
        item.forEach((element) => (element.style.zIndex = "1"));
    }

    render() {
        this.visitCard = document.createElement("div");
        this.visitCard.classList.add("visitCard");
        this.visitCard.classList.add("cardJs");
        this.visitCard.setAttribute("draggable", "true");
        this.visitCard.setAttribute("id", this.id);
        this.visitCard.addEventListener("dragstart", (event) => {
            const mainStyles = getComputedStyle(event.target);
            this.nulledIndex();
            event.target.style.zIndex = "5";
            event.target.classList.add("moved");
            event.target.style.top =
                event.target.offsetTop - parseInt(mainStyles.marginTop) + "px";
            event.target.style.left =
                event.target.offsetLeft - parseInt(mainStyles.marginLeft) + "px";
            this.offsetX =
                event.offsetX +
                rootDiv.offsetLeft +
                parseInt(mainStyles.marginLeft) +
                parseInt(mainStyles.borderWidth) * 2;
            this.offsetY =
                event.offsetY +
                rootDiv.offsetTop +
                parseInt(mainStyles.marginTop) +
                parseInt(mainStyles.borderWidth) * 2;
        });
        this.visitCard.addEventListener("dragend", (event) => {
            event.target.style.position = "absolute";
            event.target.style.top =
                event.pageY - this.offsetY < 0
                    ? 0 + "px"
                    : (event.target.style.top =
                        event.pageY - this.offsetY + event.target.offsetHeight >
                        rootDiv.offsetHeight
                            ? rootDiv.offsetHeight -
                            event.target.offsetHeight -
                            parseInt(getComputedStyle(rootDiv).paddingBottom) -
                            parseInt(getComputedStyle(event.target).marginBottom) +
                            "px"
                            : event.pageY - this.offsetY + "px");
            event.target.style.left =
                event.pageX - this.offsetX < 0
                    ? 0 + "px"
                    : (event.target.style.left =
                        event.pageX - this.offsetX + event.target.offsetWidth >
                        rootDiv.offsetWidth
                            ? rootDiv.offsetWidth -
                            event.target.offsetWidth -
                            parseInt(getComputedStyle(rootDiv).paddingRight) -
                            parseInt(getComputedStyle(event.target).marginRight) +
                            "px"
                            : event.pageX - this.offsetX + "px");
            const item = rootDiv.querySelectorAll(".moved");
            item.forEach((elem) => elem.classList.remove("moved"));
        });
        this.visitCard.innerHTML = this.createInput(
            this.idLabel,
            "id",
            this.id
        );
        this.visitCard.innerHTML += this.createInput(
            this.userNameLabel,
            "userName",
            this.userName
        );
        this.visitCard.innerHTML += this.createInput(
            this.userDocLabel,
            "userDoc",
            this.userDoc
        );
        this.visitCardInfo = document.createElement("div");
        this.visitCardInfo.classList.add("cardInfo");
        this.visitCardInfo.innerHTML = this.createInput(
            this.userVisitLabel,
            "userVisit",
            this.userVisit
        );
        this.visitCardInfo.innerHTML += this.createInput(
            this.userDescLabel,
            "userDesc",
            this.userDesc
        );
        this.visitCardInfo.innerHTML += this.createInput(
            this.priorityLabel,
            "visitPriority",
            this.visitPriority
        );
        const visitButtons = document.createElement("div");
        visitButtons.classList.add("cardBtn");
        const infoButton = document.createElement("button");
        infoButton.classList.add("moreBtn");
        infoButton.innerHTML = `more`;
        infoButton.addEventListener("click", () => {
            this.visitCard.classList.toggle("active");
            this.visitCardInfo.classList.toggle("active");
        });
        visitButtons.append(infoButton);
        const endButton = document.createElement("button");
        endButton.classList.add("delBtn");
        endButton.innerHTML = this.status;
        endButton.addEventListener("click", () => {
            if (this.status === "open") {
                this.setStatus = "done";
                endButton.innerHTML = `done`;
            } else {
                this.setStatus = "open";
                endButton.innerHTML = `open`;
            }
        });
        this.setStatus = this.status;
        const delButton = document.createElement("button");
        delButton.classList.add("delBtn");
        delButton.innerHTML = `del`;
        delButton.addEventListener("click", () => {
            deleteCard(this.id);
        });
        const editButton = document.createElement("button");
        editButton.classList.add("editBtn");
        editButton.innerHTML = `edit`;
        const openButton = document.createElement("button");
        openButton.classList.add("openEdit");
        openButton.innerHTML = `options`;
        openButton.addEventListener("click", () => {
        });
        const editButtons = document.createElement("div");
        editButtons.classList.add("editBtns");
        editButtons.classList.add("btn");
        editButtons.addEventListener("mouseover", () => {
            endButton.classList.add("focused");
            delButton.classList.add("focused");
            editButton.classList.add("focused");
            openButton.classList.add("focused");
        });
        editButtons.addEventListener("mouseout", () => {
            endButton.classList.remove("focused");
            delButton.classList.remove("focused");
            editButton.classList.remove("focused");
            openButton.classList.remove("focused");
        });
        editButtons.append(endButton);
        editButtons.append(delButton);
        editButtons.append(editButton);
        editButtons.append(openButton);
        visitButtons.append(editButtons);
        this.visitCard.append(this.visitCardInfo);
        this.visitCard.append(visitButtons);
        rootDiv.append(this.visitCard);
    }

    createInput(textLabel, textName, textValue) {
        return `<div class='cardLabel'>
                    <span>${textLabel}:</span>
                    <input type='text' name='${textName}' value='${textValue}' disabled>
                </div>`;
    }
}

class Cardiologist extends Visit {
    constructor(
        id,
        userVisit,
        userDesc,
        userDoc,
        visitPriority,
        userName,
        userPressure,
        userBodyMass,
        userIllnesses,
        userAge,
        status
    ) {
        super(id, userVisit, userDesc, userDoc, visitPriority, userName, status);
        this.userPressure = userPressure;
        this.userPressureLabel = "Pressure";
        this.userBodyMass = userBodyMass;
        this.userBodyMassLabel = "Bode Mass Index";
        this.userIllnesses = userIllnesses;
        this.userIllnessesLabel = "Clinical record";
        this.userAge = userAge;
        this.userAgeLabel = "Age";
    }

    render() {
        super.render();
        this.visitCardInfo.innerHTML += this.createInput(
            this.userPressureLabel,
            "userPressure",
            this.userPressure
        );
        this.visitCardInfo.innerHTML += this.createInput(
            this.userBodyMassLabel,
            "userBodyMass",
            this.userBodyMass
        );
        this.visitCardInfo.innerHTML += this.createInput(
            this.userIllnessesLabel,
            "userIllnesses",
            this.userIllnesses
        );
        this.visitCardInfo.innerHTML += this.createInput(
            this.userAgeLabel,
            "userAge",
            this.userAge
        );
    }
}

class Dentist extends Visit {
    constructor(
        id,
        userVisit,
        userDesc,
        userDoc,
        visitPriority,
        userName,
        lastDate,
        status
    ) {
        super(id, userVisit, userDesc, userDoc, visitPriority, userName, status);
        this.lastDate = lastDate;
        this.lastDateLabel = "Last Visit";
    }

    render() {
        super.render();
        this.visitCardInfo.innerHTML += this.createInput(
            this.lastDateLabel,
            "lastDate",
            this.lastDate
        );
    }
}

class Therapist extends Visit {
    constructor(
        id,
        userVisit,
        userDesc,
        userDoc,
        visitPriority,
        userName,
        userAge,
        status
    ) {
        super(id, userVisit, userDesc, userDoc, visitPriority, userName, status);
        this.userAge = userAge;
        this.userAgeLabel = "Age";
    }

    render() {
        super.render();
        this.visitCardInfo.innerHTML += this.createInput(
            this.userAgeLabel,
            "userAge",
            this.userAge
        );
    }
}
